[Markdown syntax guide](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

# JSF quickies #

## Java Expression Language (EL) ##
Reserved words:

*and, or, not, eq, ne, lt, gt, le, ge, true, false, null, instanceof, empty, div, mod*

Enums:
```
public enum Plays {
  Left, Right
};

#{bean.play == 'Left'} //return true
#{bean.play == 'Right'}//return false
```

Collections:
```
<c:forEach var="item" items="#{bean.collection}">
  <i>#{item}</i>,
</c:forEach>

or 

<ui:repeat var="item" value="#{bean.collection}">
  <i>#{item}</i>,
</ui:repeat>

```

EL implicit objects:


| Implicit obj. | Type | Description|
| -------------|------|------------|
| __#{application}__ |ServletContext or PortletContext | This is an instance of ServletContext or PortletContext.|
| __#{facesContext}__ | FacesContext | This is an instance of FacesContext. |
| __#{initParam}__ | Map | This is the context initialization parameter map returned by getInitParameterMap. |
| __#{session}__ |  HttpSession or PortletSession | This is an instance of HttpSession or PortletSession.|
| __#{view}__ | UIViewRoot| This is the current UIViewRoot (the root of the UIComponent tree).|
| __#{component}__ | UIComponent | This is the current UIComponent.|
| __#{cc}__ | UIComponent |  This is the composite component currently being processed.|
| __#{request}__ | ServletRequest or PortletRequest  | This is an instance of ServletRequest or PortletRequest.|
| __#{applicationScope}__ |  Map |  This is a map to store application-scoped data returned by getApplicationMap.|
| __#{sessionScope}__ |  Map | This is a map to store session-scoped data returned by getSessionMap. |
| __#{viewScope}__ |  Map | This is a map to store current view scoped data returned by getViewMap.|
| __#{requestScope}__ |  Map |  This is a map to store request-scoped data returned by getRequestMap.|
| __#{flowScope}__ | Map |  This is a map to store flow-scoped data returned by facesContext.getApplication().getFlowHandler().getCurrentFlowScope().|
| __#{flash}__ |   Map |  This is a map that contains values present only on the "next" request.|
| __#{param}__ | Map | This is a map view of all the query parameters for this request. It is returned by getRequestParameterMap.|
| __#{paramValues}__ | Map | This is the request parameter value map returned by getRequestParameterValuesMap.|
| __#{header}__ | Map | This is a map view of all the HTTP headers for this request returned by getRequestHeaderMap.|
| __#{headerValue}__ | Map |  This is the request header values map returned by getRequestHeaderValuesMap. Each value in the map is an array of strings that contains all the values for that key.|
| __#{cookie}__ | Map | This is a map view of values in the HTTP Set-Cookie header returned by getRequestCookieMap. |
| __#{resource}__ | Resource | This is a JSF resource identifier to a concrete resource URL.|


## Communication ##

### context parameters 

Context parameters are defined in the web.xml
```
<context-param>
  <param-name>key</param-name>
  <param-value>value</param-value>
</context-param>


<h:outputText value="#{initParam['key']}"/>
<h:outputText value="#{facesContext.externalContext.initParameterMap['key']}"/>

in bean:
facesContext.getExternalContext().getInitParameter("key");
```

### Passing request parameters with the <f:param> tag

```
<h:form>

  <h:commandButton value="Send"  action="#{bean.action()}">
  <f:param id="key1" name="key one" value="val_1"/>
  <f:param id="key2" name="key two" value="val_1"/>
  </h:commandButton>
</h:form>

in bean:
public String parametersAction() {

  FacesContext fc = FacesContext.getCurrentInstance();
  Map< String , String > params = fc.getExternalContext().getRequestParameterMap();
  val_1 = params.get("key1");
  val_2 = params.get("key2");
       
  return "navigate_to_page";
}
```

### View parameters

Can be used for validation or are binded to bean 

```
.xhtml?playernameparam=Rafael&playersurnameparam=Nadal 

<f:metadata>
  <f:viewParam name="playernameparam" required="true" requiredMessage="Player name required!" value="#{playersBean.playerName}"/>            
  <f:viewParam name="playersurnameparam" required="true" requiredMessage="Player surname required!" value="#{playersBean.playerSurname}"/> 
</f:metadata>

<h:body>        
  You requested name: <h:outputTextvalue="#{playersBean.playerName}"/><br/>
  You requested surname: <h:outputText value="#{playersBean.playerSurname}"/>       
</h:body>

```

### Passing attributes with the <f:attribute>

Allows you to pass the value of an attribute of a component or to pass a parameter to a component

```
<h:commandButton actionListener="#{playersBean.parametersAction}">
  <f:attribute id="playerName" name="playerNameAttr" value="Rafael"/>               
  <f:attribute id="playerSurname" name="playerSurnameAttr" value="Nadal"/>
</h:commandButton>

public void parametersAction(ActionEvent evt) {     

  playerName = (String) evt.getComponent().getAttributes().get("playerNameAttr");
  playerSurname = (String) evt.getComponent().getAttributes().get("playerSurnameAttr");
}

```

### Setting property values via action listeners

Note order of calls actionListener / action

actionListener

```
<h:commandButton value="Send Rafael Nadal 2" actionListener="#{playersBean.parametersAction}">
  <f:setPropertyActionListener id="playerName" target="#{playersBean.playerName}" value="Rafael"/>
  <f:setPropertyActionListener  id="playerSurname" target="#{playersBean.playerSurname}" value="Nadal"/>
</h:commandButton>

public void parametersAction(ActionEvent e) {        
  logger.log(Level.INFO, "Player name (from parametersAction(ActionEvent) method: {0}", playerName);
  logger.log(Level.INFO, "Player surname (from parametersAction(ActionEvent) method: {0}", playerSurname);
}

INFO:   Player name (from parametersAction() method: null
INFO:   Player surname (from parametersAction() method: null
INFO:   Player name (from setPlayerName() method: Rafael
INFO:   Player surname (from setPlayerSurname() method: Nadal

```

action

```
<h:commandButton value="Send Rafael Nadal 3" action="#{playersBean.parametersAction()}">
  <f:setPropertyActionListener id="playerName" target="#{playersBean.playerName}" value="Rafael"/>
  <f:setPropertyActionListener  id="playerSurname" target="#{playersBean.playerSurname}" value="Nadal"/>
</h:commandButton>
}

INFO:   Player name (from setPlayerName() method: Rafael
INFO:   Player surname (from setPlayerSurname() method: Nadal
INFO:   Player name (from parametersAction() method: Rafael
INFO:   Player surname (from parametersAction() method: Nadal

```

### Flash scope

variables stored in the Flash scope will be available over a redirection and they will be eliminated afterwards

useful when implementing a POST-redirect-GET pattern

Values stored in the Flash scope survive only one redirect and then are deleted.

Example Register  accout

1. register to web site (provide his name and surname and click on the Register button)
2. redirects to terms.xhtml (see above name and surname in welcome message and terms and conditions)
3. Reject button clicked: redirected index.xhtml form registration fields remains:Terms rejected! Player not registered! ``` <h:message> ```
4. Accept to done.xhtml :Terms accepted and player registered!  ``` <h:message> ``` and another message stating Name Surname successfully registered!. ``` <h:outputText> ```

```
@Named
@RequestScoped
public class PlayersBean {

   public String addValuesToFlashAction() {

    Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
    flash.put("playerName", playerName);
    flash.put("playerSurname", playerSurname);

    return "terms?faces-redirect=true";
  }

  public void pullValuesFromFlashAction(ComponentSystemEvent e) {

    Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();
    playerName = (String) flash.get("playerName");
    playerSurname = (String) flash.get("playerSurname");
  }

  public String termsAcceptedAction() {

    Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();

    flash.setKeepMessages(true);
    pullValuesFromFlashAction(null);

    //do something with firstName, lastName 
    logger.log(Level.INFO, "First name: {0}", playerName);
    logger.log(Level.INFO, "Last name: {0}", playerSurname);

    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Terms accepted and player registered!"));
    return "done?faces-redirect=true";
  }

  public String termsRejectedAction() {

    Flash flash = FacesContext.getCurrentInstance().getExternalContext().getFlash();

    flash.setKeepMessages(true);
    pullValuesFromFlashAction(null);

    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Terms rejected! Player not registered!"));
    return "index?faces-redirect=true";
  }
  
index.xhtml:
  <h:body>
    <f:metadata> 
      <f:event type="preRenderView" listener="#{playersBean.pullValuesFromFlashAction}"/> 
    </f:metadata>
    <h:messages />  
    <h:form>                       
      Name: <h:inputText value="#{playersBean.playerName}"/>
      Surname: <h:inputText value="#{playersBean.playerSurname}"/>
     <h:commandButton value="Register" action="#{playersBean.addValuesToFlashAction()}"/>          
    </h:form>
  </h:body>

  
terms.xhtml:
  <h:body>
    <h:messages />  
      Hello, <h:outputText value="#{flash.keep.playerName} #{flash.keep.playerSurname}"/>    
    <br/><br/>Terms &amp; Conditions ... ... ... ... ...
    <h:form>
    <h:commandButton value="Reject" action="#{playersBean.termsRejectedAction()}" />
    <h:commandButton value="Accept" action="#{playersBean.termsAcceptedAction()}" />
    </h:form>
  </h:body>
  
  
done.xhtml:
  <h:body>
    <f:metadata> 
     <f:event type="preRenderView" listener="#{playersBean.pullValuesFromFlashAction}"/> 
    </f:metadata>
    <h:messages />  
    <h:outputText value="#{playersBean.playerName} #{playersBean.playerSurname}"/> successfully registered!
  </h:body>  

```

### Working with hidden fields

Dealing with temporary data or information provided by the user that should be used again and again. 
JSF offers the ``` <h:inputHidden> ``` tag to pass hidden parameters

Setting hidden field values from JavaScript is a common practice.
``` 
<h:form id="hiddenFormId">
  <h:commandButton value="Send Rafael Nadal" onclick="setHiddenValues();" action="#{playersBean.parametersAction()}"/>
  <h:inputHidden id="playerName" value="#{playersBean.playerName}"/>
 <h:inputHidden id="playerSurname" value="#{playersBean.playerSurname}"/>
</h:form>

<script type="text/javascript">
  function setHiddenValues() {
    document.getElementById('hiddenFormId:playerName').value = "Rafael";
    document.getElementById('hiddenFormId:playerSurname').value = "Nadal";
  }
</script>

``` 